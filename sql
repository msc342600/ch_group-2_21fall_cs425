create table users
	(user_id 		serial  NOT NULL,
	 name		varchar(10),
	 password		varchar(10),
	 manage         integer,
	 primary key (user_id)
	);

insert into users(name,password,manage) VALUES ('xu', '123', 0);

create table document
	(document_id 		serial  NOT NULL,
	 title		varchar(50),
	 type		varchar(10) check(type in 'book','article','magazine'),
	 classification         varchar(50),
	 keyword    varchar(50),
	 primary key (document_id)
	);
 
 insert into document(title,type,classification,keyword) VALUES('Database System Concepts','book','Science-CS-Databases','databases')
 insert into document(title,type,classification,keyword) VALUES('Introduction to Algorithms','book','Science-CS-Algorithms','Algorithms')

create table book
	(document_id 		integer,
	 authors		varchar(50),
	 publisher		varchar(20),
	 editions         varchar(20),
	 primary key (document_id),
	 foreign key (document_id) references document (document_id)
	);
 insert into book(document_id,authors,publisher,editions) VALUES(1,'Silberschatz,Korth,Sudarshan','McGraw-Hill','7th')
 insert into book(document_id,authors,publisher,editions) VALUES(2,'Thomas H.Cormen','MIT','3th')

create table article
	(document_id 		integer,
	 authors		varchar(20),
	 issue         varchar(20),
	 date          varchar(20),
	 primary key (document_id),
	 foreign key (document_id) references document (document_id)
	);

create table magazine
	(document_id 		integer,
	 editors		varchar(20),
	 issue         varchar(20),
	 date          varchar(20),
	 contributors  varchar(20),
	 primary key (document_id),
	 foreign key (document_id) references document (document_id)
	);

create table borrow_info
	(borrow_id 		serial  NOT NULL,
	 strDate		varchar(20),
	 endDate		varchar(20),
	 user_id    integer,
	 document_id integer,
	 state  varchar(10),
	 primary key (borrow_id),
	 foreign key (user_id) references users (user_id),
	 foreign key (document_id) references document (document_id)
	);

create table search_info
	(search_id 		serial  NOT NULL,
	 user_id        integer,
	 words		varchar(20),
	 date		varchar(20),
	 primary key (search_id),
	 foreign key (user_id) references users (user_id)
	);

create table copy
	(copy_id 		serial  NOT NULL,
	 borrowed		varchar(20),
	 current		varchar(20),
	 document_id    integer,
	 room_id    integer,
	 primary key (document_id,copy_id),
	 foreign key (document_id) references document (document_id),
	 foreign key (room_id) references room (room_id)
	);

insert into copy(borrowed,current,document_id,room_id) VALUES('0','5',1,1)
insert into copy(borrowed,current,document_id,room_id) VALUES('0','5',2,1)


create table room
	(room_id 		serial  NOT NULL,
	 name		varchar(20),
	 level		varchar(20),
	 primary key (room_id)
	);

insert into room(name,level) VALUES('Popular Knowledge','1')


create table reference
	(document_id 		integer,
	 reference_id		integer,
	 primary key (document_id,reference_id),
	 foreign key (document_id) references document (document_id),
	 foreign key (reference_id) references document (document_id)
	);