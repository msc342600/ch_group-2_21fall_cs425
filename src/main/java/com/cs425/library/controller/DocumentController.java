package com.cs425.library.controller;

import com.cs425.library.po.*;
import com.cs425.library.po.vo.DocumentVo;
import com.cs425.library.service.DocumentService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class DocumentController {
    @Resource
    private DocumentService documentService;

    @PostMapping("/findDocument")
    public String findDocument(@Param("type") String type,
                            @Param("title") String title, @Param("classification") String classification, @Param("keyword") String keyword, HttpServletRequest request, Model model) {
        List<DocumentVo> documents = documentService.selectDocumentByInfo(title, type, classification, keyword);

        request.getSession().setAttribute("type", type);
        model.addAttribute("documentsList", documents);
        User user = (User)request.getSession().getAttribute("user");
        if (user.getManage() == 1){
            return "admin/manageDocument";
        }
        return "findBook";
    }

    @PostMapping("/findDocumentByID")
    public String findDocumentById(@Param("documentId") Integer documentId, Model model,HttpServletRequest request) {
        DocumentVo documentVo = new DocumentVo();
        Document documents = documentService.selectDocumentById(documentId);
        Book book = documentService.selectBookById(documentId);
        Article article = documentService.selectArticleById(documentId);
        Magazine magazine = documentService.selectMagazineById(documentId);

        documentVo.setDocumentId(documentId);
        documentVo.setTitle(documents.getTitle());
        documentVo.setType(documents.getType());
        documentVo.setKeyword(documents.getKeyword());
        documentVo.setClassification(documents.getClassification());
        if(book != null){
            documentVo.setBookPublisher(book.getPublisher());
            documentVo.setBookAuthors(book.getAuthors());
            documentVo.setBookEditions(book.getEditions());
        }else if (article != null){
            documentVo.setArticleDate(article.getDate());
            documentVo.setArticleIssue(article.getIssue());
            documentVo.setArticleAuthors(article.getAuthors());
        }else if (magazine != null){
            documentVo.setMagazineIssue(magazine.getIssue());
            documentVo.setMagazineContributors(magazine.getContributors());
            documentVo.setMagazineEditors(magazine.getEditors());
            documentVo.setMagazineDate(magazine.getDate());
        }
        Copy copy = documentService.selectCopyByDocument(documentId);
        documentVo.setCapacity(copy.getCurrent());
        documentVo.setRoom(String.valueOf(copy.getRoomId()));
        model.addAttribute("documents", documentVo);
        User user = (User)request.getSession().getAttribute("user");
        if (user.getManage() == 1){
            return "admin/manageDocument";
        }
        return "bookDocument";
    }
    @GetMapping("/checkOverdueDocuments")
    public List<Document> checkOverdueDocuments(@Param("documentId") Integer documentId) {
        return documentService.checkOverdueDocuments(documentId);
    }


    @PostMapping("/addCopy")
    public void addCopy(HttpServletRequest request) {
        documentService.addCopy(request);
    }

    @PostMapping("/modifyCopy")
    public void modifyCopy(@RequestBody Copy copy) {
        if (copy.getCopyId() != null) {
            documentService.modifyCopy(copy);
        }
    }

    @DeleteMapping("/deleteCopyById/{copyId}")
    public void deleteCopyById(@PathVariable("copyId") Integer copyId) {
        if (copyId != null) {
            documentService.deleteCopyById(copyId);
        }
    }


}
