package com.cs425.library.controller;

import com.cs425.library.po.User;
import com.cs425.library.po.vo.BookRecordVo;
import com.cs425.library.service.UserService;
import com.cs425.library.service.impl.BookRecordServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {
    @Resource
    private UserService userService;
    @Resource
    private BookRecordServiceImpl bookRecordService;

    @PostMapping("/userLogin")
    public String userLogin(@Param("userName") String userName,
                            @Param("password") String password, HttpServletRequest request) {
        User user = userService.userLogin(userName, password);
        request.getSession().setAttribute("user", user);
        if (null != user) {
            if (user.getManage() == 1) {
                return "admin/admin";
            }
            return "user";
        }
        return "index";
    }

    @RequestMapping("/userIndex")
    public String userIndex() {
        return "index";
    }

    @RequestMapping("/userLogOut")
    public String userLogOut(HttpServletRequest request) {
        request.getSession().invalidate();
        return "index";
    }

    @RequestMapping("/findBookPage")
    public String findBookPage() {
        return "findBook";
    }

    @RequestMapping("/borrowingPage")
    public String borrowing() {
        return "bookDocument";
    }

    @RequestMapping("/userBorrowingBook")
    @ResponseBody
    public boolean borrowingBook(int bookId, HttpServletRequest request) {
        return userService.userBorrowingBook(bookId, request);
    }

    @RequestMapping("/userBorrowBookRecord")
    public String userBorrowBookRecord(Model model, HttpServletRequest request) {
        ArrayList<BookRecordVo> res = bookRecordService.selectAllBookRecord(request);
        model.addAttribute("borrowInfos", res);
        return "bookRecord";
    }

    @PostMapping("/findBookRecordById")
    public String findBookRecordById(@Param("bookId")int bookId, Model model) {
        BookRecordVo res = bookRecordService.selectBookRecordById(bookId);
        model.addAttribute("borrowInfos", res);
        return "returnDocument";
    }

    @RequestMapping("/userReturnBook")
    @ResponseBody
    public boolean returnBook(int bookId, HttpServletRequest request) {
        return userService.userReturnBook(bookId, request);
    }

    @RequestMapping("/userReturnBooksPage")
    public String userReturnBooksPage() {
        return "returnDocument";
    }




    @DeleteMapping("/deleteUserById/{userId}")
    public void deleteUser(@PathVariable("userId") Integer userId) {
        if (userId != 0) {
            userService.deleteUserById(userId);
        }
    }
}
