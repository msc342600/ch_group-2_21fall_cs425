package com.cs425.library.controller;

import com.cs425.library.po.User;
import com.cs425.library.po.vo.BookRecordVo;
import com.cs425.library.service.DocumentService;
import com.cs425.library.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {
    @Resource
    UserService userService;
    @Resource
    DocumentService documentService;

    @PostMapping("/addUser")
    @ResponseBody
    public String addUser(User user) {
        int res = userService.addUser(user);
        if (res > 0){
            return "true";
        }
        return "false";
    }

    @RequestMapping("/addUserPage")
    public String addUserPage() {
        return "admin/addUser";
    }

    @RequestMapping("/findUserPage")
    public String showUsersPage() { return "admin/showUsers"; }

    @RequestMapping("/findUser")
    public String showUsers(Model model, @Param("name") String name) {
        List<User> users = userService.findUserList(name);
        model.addAttribute("users", users);
        return "admin/showUsers";
    }

    @RequestMapping("/addDocumentPage")
    public String addDocumentPage() { return "admin/addDocument"; }


    @RequestMapping("/addDocument")
    @ResponseBody
    public String addDocument(HttpServletRequest request) {
        String res = documentService.addDocument(request);
        return res; }

    @RequestMapping("/modifyBookPage")
    public String modifyBookPage() { return "admin/manageDocument"; }

    @RequestMapping("/modifyBook")
    @ResponseBody
    public String modifyBook(HttpServletRequest request) {
        String res = documentService.modifyDocument(request);
        return res; }

    @RequestMapping("/deleteBook")
    @ResponseBody
    public String deleteBook(HttpServletRequest request) {
        String res = documentService.deleteDocumentById(request);
        return res; }
}
