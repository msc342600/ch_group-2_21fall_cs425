package com.cs425.library.service;

import com.cs425.library.po.BorrowInfo;
import com.cs425.library.po.vo.BookRecordVo;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public interface BookRecordService {
    public ArrayList<BookRecordVo> selectAllBookRecord(HttpServletRequest request);
    public BookRecordVo selectBookRecordById(int bookId);
}
