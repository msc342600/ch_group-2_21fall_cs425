package com.cs425.library.service;

import com.cs425.library.po.BorrowInfo;
import com.cs425.library.po.Room;
import com.cs425.library.po.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {
    // find users by name
    List<User> findUserByUserName(String userName);

    //login
    User userLogin(String userName, String password);

    //find user borrow info
    List<BorrowInfo> findAllBorrowingBooks(HttpServletRequest request);

    //return boot
    boolean userReturnBook(int bookId, HttpServletRequest request);

    //borrow book
    boolean userBorrowingBook(int bookId, HttpServletRequest request);

    // find all users
    List<User> findUserList(String name);

    // add user
    int addUser(User user);

    // delete user by userId
    void deleteUserById(Integer userId);

    Room findRoomById(Integer roomId);
}
