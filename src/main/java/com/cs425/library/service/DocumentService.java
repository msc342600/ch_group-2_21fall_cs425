package com.cs425.library.service;

import com.cs425.library.po.*;
import com.cs425.library.po.vo.DocumentVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface DocumentService {
    //find document by title
    List<DocumentVo> selectDocumentByInfo(String title, String type, String classification, String keyword);

    Document selectDocumentById(Integer id);

    Copy selectCopyByDocument(Integer id);

    Book selectBookById(Integer id);

    Magazine selectMagazineById(Integer id);

    Article selectArticleById(Integer id);

    String addDocument(HttpServletRequest request);

    String modifyDocument(HttpServletRequest request);

    String deleteDocumentById(HttpServletRequest request);

    void addCopy(HttpServletRequest request);

    void modifyCopy(Copy copy);

    void deleteCopyById(Integer copyId);

    List<Document> checkOverdueDocuments(Integer documentId);
}
