package com.cs425.library.service.impl;

import com.cs425.library.dao.*;
import com.cs425.library.po.*;
import com.cs425.library.po.vo.DocumentVo;
import com.cs425.library.service.DocumentService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {
    @Resource
    public DocumentMapper documentMapper;

    @Resource
    public CopyMapper copyMapper;

    @Resource
    public BookMapper bookMapper;

    @Resource
    public MagazineMapper magazineMapper;

    @Resource
    public ArticleMapper articleMapper;

    @Resource
    public RoomMapper roomMapper;

    @Resource
    public BorrowInfoMapper borrowInfoMapper;

    @Override
    public List<DocumentVo> selectDocumentByInfo(String title, String type, String classification, String keyword) {
        List<Document> documents = documentMapper.selectDocumentByInfo(title, type, classification, keyword);
        List<DocumentVo> documentVos = new LinkedList<>();

        for (Document d : documents) {
            DocumentVo documentVo = new DocumentVo();
            documentVo.setDocumentId(d.getDocumentId());
            documentVo.setType(d.getType());
            documentVo.setTitle(d.getTitle());
            documentVo.setClassification(d.getClassification());
            documentVo.setKeyword(d.getKeyword());
         if(d.getType().equals("book")){
             Book book = selectBookById(d.getDocumentId());
             documentVo.setBookAuthors(book.getAuthors());
             documentVo.setBookEditions(book.getEditions());
             documentVo.setBookPublisher(book.getPublisher());
         }else if(d.getType().equals("magazine")){
             Magazine magazine = selectMagazineById(d.getDocumentId());
             documentVo.setMagazineDate(magazine.getDate());
             documentVo.setMagazineContributors(magazine.getContributors());
             documentVo.setMagazineEditors(magazine.getEditors());
             documentVo.setMagazineIssue(magazine.getIssue());
         }else {
             Article article = selectArticleById(d.getDocumentId());
             documentVo.setArticleAuthors(article.getAuthors());
             documentVo.setArticleIssue(article.getIssue());
             documentVo.setArticleDate(article.getDate());
        }
            Copy copy = selectCopyByDocument(d.getDocumentId());
            documentVo.setCapacity(copy.getCurrent());
            Room room = roomMapper.selectByPrimaryKey(copy.getRoomId());
            documentVo.setRoom(room.getName());
            documentVos.add(documentVo);
        }

        return documentVos;
    }

    public Document selectDocumentById(Integer id){
        return documentMapper.selectByPrimaryKey(id);
    }

    @Override
    public Copy selectCopyByDocument(Integer id) {
        Copy copy = copyMapper.selectByDocumentID(id);
        String capacity = copy.getCurrent();
        if (capacity.equals("")) {
            copy.setCurrent("0");
        }
        return copy;
    }

    @Override
    public Book selectBookById(Integer id) {
        return bookMapper.selectByPrimaryKey(id);
    }

    @Override
    public Magazine selectMagazineById(Integer id) {
        return magazineMapper.selectByPrimaryKey(id);
    }

    @Override
    public Article selectArticleById(Integer id) {
        return articleMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Document> checkOverdueDocuments(Integer documentId) {
        List<Document> documents = new ArrayList<>();
        String now = String.valueOf(System.currentTimeMillis());
        int count = borrowInfoMapper.countOverdueDocuments(documentId, now);
        if (count == 0) {
            return documents;
        }
        return borrowInfoMapper.checkOverdueDocuments(documentId, now);
    }

    @Override
    public String addDocument(HttpServletRequest request) {

            Document document = new Document();
            document.setTitle(request.getParameter("title"));
            document.setType(request.getParameter("type"));
            document.setClassification(request.getParameter("classification"));
            document.setKeyword(request.getParameter("keyword"));
            int a = documentMapper.insertSelective(document);
            int b = 0;
            int documentId = document.getDocumentId();
            if ("book".equals(request.getParameter("type"))) {
                Book book = new Book();
                book.setDocumentId(documentId);
                book.setAuthors(request.getParameter("authors"));
                book.setPublisher(request.getParameter("publisher"));
                book.setEditions(request.getParameter("editions"));
                b = bookMapper.insertSelective(book);
            } else if ("magazine".equals(request.getParameter("type"))) {
                Magazine magazine = new Magazine();
                magazine.setDocumentId(documentId);
                magazine.setEditors(request.getParameter("editors"));
                magazine.setIssue(request.getParameter("issue"));
                magazine.setDate(request.getParameter("date"));
                magazine.setContributors(request.getParameter("contributors"));
                b = magazineMapper.insertSelective(magazine);
            } else if ("article".equals(request.getParameter("type"))) {
                Article article = new Article();
                article.setDocumentId(documentId);
                article.setAuthors(request.getParameter("authors"));
                article.setIssue(request.getParameter("issue"));
                article.setDate(request.getParameter("date"));
                b = articleMapper.insertSelective(article);
            }
            Copy copy = new Copy();
            copy.setDocumentId(documentId);
            copy.setBorrowed("0");
            copy.setCurrent(request.getParameter("copy"));
            copy.setRoomId(Integer.valueOf(request.getParameter("room")));
            int c = copyMapper.insertSelective(copy);
            if (a>0 && b>0 && c>0){
                return "true";
            }
        return "false";
    }

    @Override
    public String  modifyDocument(HttpServletRequest request) {
        Document document = new Document();
        document.setDocumentId(Integer.valueOf(request.getParameter("id")));
        document.setTitle(request.getParameter("title"));
        document.setType(request.getParameter("type"));
        document.setClassification(request.getParameter("classification"));
        document.setKeyword(request.getParameter("keyword"));
        int a = documentMapper.updateByPrimaryKeySelective(document);
        int b = 0;
        int documentId = document.getDocumentId();
        if ("book".equals(request.getParameter("type"))) {
            Book book = new Book();
            book.setDocumentId(documentId);
            book.setAuthors(request.getParameter("authors"));
            book.setPublisher(request.getParameter("publisher"));
            book.setEditions(request.getParameter("editions"));
            b = bookMapper.updateByPrimaryKeySelective(book);
        } else if ("magazine".equals(request.getParameter("type"))) {
            Magazine magazine = new Magazine();
            magazine.setDocumentId(documentId);
            magazine.setEditors(request.getParameter("editors"));
            magazine.setIssue(request.getParameter("issue"));
            magazine.setDate(request.getParameter("date"));
            magazine.setContributors(request.getParameter("contributors"));
            b = magazineMapper.updateByPrimaryKeySelective(magazine);
        } else if ("article".equals(request.getParameter("type"))) {
            Article article = new Article();
            article.setDocumentId(documentId);
            article.setAuthors(request.getParameter("authors"));
            article.setIssue(request.getParameter("issue"));
            article.setDate(request.getParameter("date"));
            b = articleMapper.updateByPrimaryKeySelective(article);
        }
        Copy copy = new Copy();
        copy.setDocumentId(documentId);
        copy.setCurrent(request.getParameter("copy"));
        copy.setRoomId(Integer.valueOf(request.getParameter("room")));
        int c = copyMapper.updateByDocumentId(copy);
        if (a>0 && b>0 && c>0){
            return "true";
        }
        return "false";
    }

    @Override
    public String deleteDocumentById(HttpServletRequest request) {
        int documentId = Integer.valueOf(request.getParameter("id"));
            copyMapper.deleteByDocumentId(documentId);
        if ("book".equals(request.getParameter("type"))) {
            bookMapper.deleteByPrimaryKey(documentId);
        }else if("magazine".equals(request.getParameter("type"))){
            magazineMapper.deleteByPrimaryKey(documentId);
        }else if("article".equals(request.getParameter("type"))) {
            articleMapper.deleteByPrimaryKey(documentId);
        }
        documentMapper.deleteByPrimaryKey(documentId);
        return "ture";
    }

    @Override
    public void addCopy(HttpServletRequest request) {
        if (request != null) {
            Integer documentId = Integer.valueOf(request.getParameter("documentId"));
            Copy copy = new Copy();
            copy.setDocumentId(documentId);
            copy.setCopyId(documentId);
            copy.setCurrent(request.getParameter("current"));
            copy.setBorrowed("0");
        }
    }

    @Override
    public void modifyCopy(Copy copy) {
        if (copy != null) {
            copyMapper.updateByPrimaryKeySelective(copy);
        }
    }

    @Override
    public void deleteCopyById(Integer copyId) {
        if (copyId != null) {
            copyMapper.deleteByPrimaryKey(copyId);
        }
    }


}
