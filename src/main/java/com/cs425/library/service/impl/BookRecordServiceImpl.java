package com.cs425.library.service.impl;

import com.cs425.library.dao.BorrowInfoMapper;
import com.cs425.library.dao.DocumentMapper;
import com.cs425.library.po.BorrowInfo;
import com.cs425.library.po.Document;
import com.cs425.library.po.User;
import com.cs425.library.po.vo.BookRecordVo;
import com.cs425.library.service.BookRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookRecordServiceImpl implements BookRecordService {
    @Resource
    private BorrowInfoMapper borrowInfoMapper;
    @Resource
    private DocumentMapper documentMapper;

    @Override
    public BookRecordVo selectBookRecordById(int bookId) {
        BorrowInfo borrowInfo = borrowInfoMapper.selectByPrimaryKey(bookId);
            BookRecordVo bookRecordVo = new BookRecordVo();
            Document document = documentMapper.selectByPrimaryKey(borrowInfo.getDocumentId());
            bookRecordVo.setBorrowId(borrowInfo.getBorrowId());
            bookRecordVo.setTitle(document.getTitle());
            bookRecordVo.setStrdate(borrowInfo.getStrdate());
            bookRecordVo.setEnddate(borrowInfo.getEnddate());
            bookRecordVo.setState(borrowInfo.getState());
        return bookRecordVo;
    }

    @Override
    public ArrayList<BookRecordVo> selectAllBookRecord(HttpServletRequest request) {
        ArrayList<BookRecordVo> bookRecordVos = new ArrayList<BookRecordVo>();
        User user = (User)request.getSession().getAttribute("user");
        List<BorrowInfo> borrowInfos = borrowInfoMapper.selectByUserId(user.getUserId());
        for(BorrowInfo b:borrowInfos){
            BookRecordVo bookRecordVo = new BookRecordVo();
            Document document = documentMapper.selectByPrimaryKey(b.getDocumentId());
            bookRecordVo.setBorrowId(b.getBorrowId());
            bookRecordVo.setTitle(document.getTitle());
            bookRecordVo.setStrdate(b.getStrdate());
            bookRecordVo.setEnddate(b.getEnddate());
            bookRecordVo.setState(b.getState());
            bookRecordVos.add(bookRecordVo);
        }

        return bookRecordVos;
    }
}
