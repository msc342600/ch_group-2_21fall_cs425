package com.cs425.library.service.impl;

import com.cs425.library.dao.*;
import com.cs425.library.po.*;
import com.cs425.library.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Resource
    private BorrowInfoMapper borrowInfoMapper;

    @Resource
    private CopyMapper copyMapper;

    @Resource
    private RoomMapper roomMapper;




    @Override
    public List<User> findUserByUserName(String name) {
        List<User> user = userMapper.selectByUserName(name);
        return user;
    }

    @Override
    public User userLogin(String userName, String password) {
        List<User> users = findUserByUserName(userName);

        if (null == users) {
            return null;
        }
        for (User user : users) {
            if (user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }


    @Override
    public List<BorrowInfo> findAllBorrowingBooks(HttpServletRequest request) {
        return null;
    }

    @Override
    public boolean userReturnBook(int bookId, HttpServletRequest request) {
        BorrowInfo borrowInfo = borrowInfoMapper.selectByPrimaryKey(bookId);
        Copy copy = copyMapper.selectByDocumentID(borrowInfo.getDocumentId());
        if(borrowInfo.getState().equals("Return")){
            return false;
        }
        try {
            borrowInfo.setState("Return");
            int current = Integer.parseInt(copy.getCurrent());
            int borrowed = Integer.parseInt(copy.getBorrowed());
            int newCurrent = current + 1 ;
            int newBorrowed = borrowed - 1;
            copy.setCurrent(String.valueOf(newCurrent));
            copy.setBorrowed(String.valueOf(newBorrowed));
            copyMapper.updateByPrimaryKeySelective(copy);
            borrowInfoMapper.updateByPrimaryKey(borrowInfo);
        }catch (Exception e){
            return false;
        }
        return true;
    }


    @Override
    public boolean userBorrowingBook(int bookId, HttpServletRequest request) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String startTime = sdf.format(cal.getTime());
        cal.add(Calendar.MONTH,1);
        String endTime = sdf.format(cal.getTime());
        Copy copy = copyMapper.selectByDocumentID(bookId);
        int current = Integer.parseInt(copy.getCurrent());
        int borrowed = Integer.parseInt(copy.getBorrowed());
        if(current == 0){
            return false;
        };
        try{
            int newCurrent = current - 1 ;
            int newBorrowed = borrowed + 1;
            copy.setCurrent(String.valueOf(newCurrent));
            copy.setBorrowed(String.valueOf(newBorrowed));
            copyMapper.updateByPrimaryKeySelective(copy);

            // insert borrow info
            BorrowInfo borrowInfo = new BorrowInfo();
            borrowInfo.setDocumentId(copy.getDocumentId());
            User user = (User)request.getSession().getAttribute("user");
            borrowInfo.setUserId(user.getUserId());
            borrowInfo.setStrdate(startTime);
            borrowInfo.setEnddate(endTime);
            borrowInfo.setState("Booking");
            int n = borrowInfoMapper.insert(borrowInfo);
            if (n > 0){
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    @Override
    public List<User> findUserList(String name) {
        return userMapper.findUserList(name);
    }

    @Override
    public int addUser(User user) {
            return userMapper.insert(user);
        }

    @Override
    public void deleteUserById(Integer userId) {
        if (userId != 0) {
            userMapper.deleteByPrimaryKey(userId);
        }
    }
    @Override
    public Room findRoomById(Integer roomId) {
        return roomMapper.selectByPrimaryKey(roomId);
    }
}
