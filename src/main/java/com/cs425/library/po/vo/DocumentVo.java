package com.cs425.library.po.vo;
import lombok.Data;
@Data
public class DocumentVo {
    private Integer documentId;
    private String title;
    private String type;
    private String classification;
    private String keyword;

    //article view object
    private String articleAuthors;
    private String articleIssue;
    private String articleDate;
    //book view object
    private String bookAuthors;
    private String bookPublisher;
    private String bookEditions;
    //magazine view object
    private String magazineIssue;
    private String magazineDate;
    private String magazineEditors;
    private String magazineContributors;
    //other view object
    private String capacity;
    private String room;
    private String reference;
}
