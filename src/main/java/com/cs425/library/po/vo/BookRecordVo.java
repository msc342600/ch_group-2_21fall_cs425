package com.cs425.library.po.vo;

import lombok.Data;

@Data
public class BookRecordVo {
    private Integer borrowId;

    private String strdate;

    private String enddate;

    private String title;

    private String state;
}
