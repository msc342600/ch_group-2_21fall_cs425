package com.cs425.library.po;

public class Book {
    private Integer documentId;

    private String authors;

    private String publisher;

    private String editions;

    public Integer getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors == null ? null : authors.trim();
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher == null ? null : publisher.trim();
    }

    public String getEditions() {
        return editions;
    }

    public void setEditions(String editions) {
        this.editions = editions == null ? null : editions.trim();
    }
}