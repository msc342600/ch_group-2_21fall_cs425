package com.cs425.library.dao;

import com.cs425.library.po.Magazine;

public interface MagazineMapper {
    int deleteByPrimaryKey(Integer documentId);

    int insert(Magazine record);

    int insertSelective(Magazine record);

    Magazine selectByPrimaryKey(Integer documentId);

    int updateByPrimaryKeySelective(Magazine record);

    int updateByPrimaryKey(Magazine record);
}