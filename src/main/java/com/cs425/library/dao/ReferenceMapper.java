package com.cs425.library.dao;

import com.cs425.library.po.ReferenceKey;

public interface ReferenceMapper {
    int deleteByPrimaryKey(ReferenceKey key);

    int insert(ReferenceKey record);

    int insertSelective(ReferenceKey record);
}