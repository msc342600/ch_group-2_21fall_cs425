package com.cs425.library.dao;

import com.cs425.library.po.Copy;

public interface CopyMapper {
    int deleteByPrimaryKey(Integer copyId);

    int deleteByDocumentId(Integer copyId);

    int insert(Copy record);

    int insertSelective(Copy record);

    Copy selectByPrimaryKey(Integer copyId);

    Copy selectByDocumentID(Integer documentId);

    int updateByPrimaryKeySelective(Copy record);

    int updateByDocumentId(Copy record);

    int updateByPrimaryKey(Copy record);
}