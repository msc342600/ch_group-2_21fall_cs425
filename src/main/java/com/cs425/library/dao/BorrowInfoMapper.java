package com.cs425.library.dao;

import com.cs425.library.po.BorrowInfo;
import com.cs425.library.po.Document;
import com.cs425.library.po.vo.BookRecordVo;

import java.util.ArrayList;
import java.util.List;

public interface BorrowInfoMapper {
    int deleteByPrimaryKey(Integer borrowId);

    int insert(BorrowInfo record);

    int insertSelective(BorrowInfo record);

    BorrowInfo selectByPrimaryKey(Integer borrowId);

    ArrayList<BorrowInfo> selectByUserId(Integer user_id);

    int updateByPrimaryKeySelective(BorrowInfo record);

    int updateByPrimaryKey(BorrowInfo record);

    List<Document> checkOverdueDocuments(Integer documentId, String now);

    int countOverdueDocuments(Integer documentId, String now);
}