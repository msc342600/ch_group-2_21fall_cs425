package com.cs425.library.dao;

import com.cs425.library.po.Article;

public interface ArticleMapper {
    int deleteByPrimaryKey(Integer documentId);

    int insert(Article record);

    int insertSelective(Article record);

    Article selectByPrimaryKey(Integer documentId);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKey(Article record);
}