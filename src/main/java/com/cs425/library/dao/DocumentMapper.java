package com.cs425.library.dao;

import com.cs425.library.po.Document;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DocumentMapper {
    int deleteByPrimaryKey(Integer documentId);

    int insert(Document record);

    int insertSelective(Document record);

    Document selectByPrimaryKey(Integer documentId);

    List<Document> selectDocumentByInfo(@Param("title") String title, @Param("type")String type, @Param("classification")String classification, @Param("keyword")String keyword);

    int updateByPrimaryKeySelective(Document record);

    int updateByPrimaryKey(Document record);
}