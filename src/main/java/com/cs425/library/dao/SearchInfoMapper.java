package com.cs425.library.dao;

import com.cs425.library.po.SearchInfo;

public interface SearchInfoMapper {
    int deleteByPrimaryKey(Integer searchId);

    int insert(SearchInfo record);

    int insertSelective(SearchInfo record);

    SearchInfo selectByPrimaryKey(Integer searchId);

    int updateByPrimaryKeySelective(SearchInfo record);

    int updateByPrimaryKey(SearchInfo record);
}