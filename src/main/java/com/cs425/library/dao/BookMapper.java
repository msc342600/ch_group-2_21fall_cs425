package com.cs425.library.dao;

import com.cs425.library.po.Book;

public interface BookMapper {
    int deleteByPrimaryKey(Integer documentId);

    int insert(Book record);

    int insertSelective(Book record);

    Book selectByPrimaryKey(Integer documentId);

    int updateByPrimaryKeySelective(Book record);

    int updateByPrimaryKey(Book record);
}